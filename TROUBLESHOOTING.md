# Troubleshooting

Make sure that:

- all the three LEDs (J0-J2) on the [QPID Data Acquisition (DAQ) device](https://docs.quanser.com/quarc/documentation/qpid.html) are ON. If they are not, check if all the three PCI connectors are correctly plugged in to the PC (on its back side). Occasionally somebody pulls them out as the PC is shared with some other experiment. 

- the green LED on the back side of the experiment is on. If it is off and the red one is on instead, it means that the safety LIMIT switch has been engaged. This can be easily overlooked as the LED is on the back side, but this situation can be easily recognized by seeing the sensors read while the actuators stay still. In order to correct this, move (or make sure that) the lower (road-level) stage is roughly in the middle of its range and then press the LIMIT button on the back side of the experiment (just next to the LEDs). 

- the red LEDs on the back side of the amplifier are off. If they are on, the amplifier has been cut off by the safety RED BUTTON. Make sure the button is unlocked (twisting it following the graphical signs on it).  

- the QUARC configuration block named [HIL Initialize](https://docs.quanser.com/quarc/documentation/hil_initialize_block.html) in the (upper right corner of the) diagram is correctly configured to the `QPID` board. If you downloaded the files from the Quanser website instead of using the fixed code provided here, most probably it will be set to `Q8` board by default. 

- the target is set `quanser_win64.tlc` (again, by default after downloading from the web this will probably be `quanser_windows.tlc`, which is a 32-bit version).